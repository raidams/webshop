package de.hsesslingen.webshopbackend.shoppingcart;

import java.util.*;

import org.springframework.beans.factory.annotation.Value;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.hsesslingen.webshopbackend.item.Item;

import org.springframework.beans.factory.annotation.Autowired;

@CrossOrigin(origins = "http://localhost:8213")
@RestController
@RequestMapping("/api-riaiit00/shopping-cart-management/v1")
@Service("ShoppingCartController")
public class ShoppingCartController {

  //HashMap<String,ShoppingCart> ShoppingCartMapInMemory = new HashMap<String,ShoppingCart>();
@Autowired
ShoppingCartRepository ShoppingCartRepository;

  @Value("${server.port}")
  String port;

  @PostMapping("/GetShoppingCart")
  public ResponseEntity<ShoppingCart>getShoppingCart(@RequestParam(required = false) String sessionID) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set("server-port", port);

    Optional<ShoppingCart> ShoppingCart = ShoppingCartRepository.findById(sessionID);

    if (ShoppingCart.isPresent()) {
      return new ResponseEntity<>(ShoppingCart.get(), responseHeaders, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/PutItemsToShoppingCart")
  public ResponseEntity<ShoppingCart> putItemsToShoppingCart(@RequestParam(required = false) String sessionID, @RequestBody List<Item> items) {
    
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set("server-port", port);
    
    ShoppingCart _ShoppingCart = new ShoppingCart(sessionID, items);
    ShoppingCartRepository.save(_ShoppingCart);
    
    return new ResponseEntity<>(_ShoppingCart, responseHeaders, HttpStatus.CREATED);
  }

  @PostMapping("/DeleteShoppingCart")
  public ResponseEntity<HttpStatus> deleteShoppingCart(@RequestParam(required = false) String sessionID) {
  
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set("server-port", port);

    try {
      ShoppingCartRepository.deleteById(sessionID);;
      return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
