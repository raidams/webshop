FROM node:20.11.0-alpine

RUN apk add --no-cache openjdk11 maven \    
    && apk update \
    && apk upgrade 

RUN npm install -g @vue/cli 

# Set Java environment variables
ENV JAVA_HOME=/usr/lib/jvm/default-jvm
ENV PATH="$PATH:$JAVA_HOME/bin"