import Vue from 'vue';
import App from './App.vue';
import router from './router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faHome
} from '@fortawesome/free-solid-svg-icons';
import authentication from "@/plugins/authentication";
import VeeValidate from 'vee-validate';


	
library.add(faHome);

Vue.use(VeeValidate);

Vue.config.productionTip = false;
	
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(authentication);
	
Vue.$keycloak
  .init({ checkLoginIframe: false })
  .then(() => {
    new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  });

