package de.hsesslingen.webshopbackend.item;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import de.hsesslingen.webshopbackend.security.JWTHelper;

import org.springframework.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Value;

import jakarta.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.annotation.Transactional;


@CrossOrigin(origins = "http://localhost:8113")
@RestController
@RequestMapping("/api-riaiit00/item-management/v1/")


public class ItemController {

  @Autowired
  ItemService itemService;
  
  @Value("${server.port}")
  String port;

	@Autowired
	ItemRepository itemRepository;

  @Autowired
  JmsTemplate jmsTemplate;

  @Value("${item.queue}")
  String queue;

  private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);

  HttpHeaders responseHeaders = new HttpHeaders();

	@GetMapping("/items")
	public ResponseEntity<List<Item>> getAllItems(@RequestParam(required = false) String title) {
		
    responseHeaders.set("server-port", port);

		try {
			List<Item> items = new ArrayList<Item>();

			if (title == null)
				itemRepository.findAll().forEach(items::add);
			else
				itemRepository.findByTitleContaining(title).forEach(items::add);

			if (items.isEmpty()) {
				return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(items, responseHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

@GetMapping("/items/{id}")
public ResponseEntity<Item> getItemById(@PathVariable("id") long id) {

  HttpHeaders responseHeaders = new HttpHeaders();
  responseHeaders.set("server-port", port);

  Optional<Item> itemData = itemService.findById(id);

  if (itemData.isPresent()) {
    return new ResponseEntity<>(itemData.get(), responseHeaders, HttpStatus.OK);
  } else {
    return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
  }
}

@PostMapping("/items")
@Transactional
public ResponseEntity<Item> createItem(@AuthenticationPrincipal Jwt jwt, @Valid @RequestBody Item item) {

  jmsTemplate.setSessionTransacted(true);
  try {
    JWTHelper jwtHelper = new JWTHelper();
    if (!jwtHelper.isAdmin(jwt)) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    } else {
      LOGGER.info("try to create database entry for '{}'",item.toJSONString());
      Item _item = itemRepository
        .save(new Item(item.getTitle(), item.getDescription(), item.getPrice(), item.getAvailability()));
      LOGGER.info("created database entry for '{}'",item.toJSONString());

      LOGGER.info("try to send message '{}'", item.toJSONString());
      jmsTemplate.convertAndSend(queue, item.toJSONString());
      LOGGER.info("message sent '{}'", item.toJSONString());

      return new ResponseEntity<>(_item, HttpStatus.CREATED);
    }
  } catch (Exception e) {
    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}

@PutMapping("/items/{id}")
public ResponseEntity<Item> updateItem(@AuthenticationPrincipal Jwt jwt, @PathVariable("id") long id, @Valid @RequestBody Item item) {
      responseHeaders.set("server-port", port);

  JWTHelper jwtHelper = new JWTHelper();
  if (!jwtHelper.isAdmin(jwt)) {
    return new ResponseEntity<>(responseHeaders, HttpStatus.FORBIDDEN);
  } else {

    Optional<Item> itemData = itemRepository.findById(id);

    if (itemData.isPresent()) {
      Item _item = itemData.get();
      _item.setTitle(item.getTitle());
      _item.setDescription(item.getDescription());
      _item.setPrice(item.getPrice());
      _item.setAvailability(item.getAvailability());
      return new ResponseEntity<>(itemRepository.save(_item), responseHeaders, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
    }
  }
}

@DeleteMapping("/items/{id}")
public ResponseEntity<HttpStatus> deleteItem(@AuthenticationPrincipal Jwt jwt, @PathVariable("id") long id) {
  responseHeaders.set("server-port", port);

  try {
    JWTHelper jwtHelper = new JWTHelper();
    if (!jwtHelper.isAdmin(jwt)) {
      return new ResponseEntity<>(responseHeaders, HttpStatus.FORBIDDEN);
    } else {
      itemRepository.deleteById(id);
      return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    }
  } catch (Exception e) {
    return new ResponseEntity<>(responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
@DeleteMapping("/items")
public ResponseEntity<HttpStatus> deleteAllItems(@AuthenticationPrincipal Jwt jwt) {
  responseHeaders.set("server-port", port);

  try {
    JWTHelper jwtHelper = new JWTHelper();
    if (!jwtHelper.isAdmin(jwt)) {
      return new ResponseEntity<>(responseHeaders, HttpStatus.FORBIDDEN);
    } else {
      itemRepository.deleteAll();
      return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    }
  } catch (Exception e) {
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

}

}