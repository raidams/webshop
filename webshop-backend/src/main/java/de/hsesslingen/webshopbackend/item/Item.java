package de.hsesslingen.webshopbackend.item;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;

@Entity
@Table(name = "item")
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    @NotBlank(message = "Title is mandatory")
    private String title;

    @Column(name = "description")
    @NotBlank(message = "Description is mandatory")
    private String description;

    @Column(name = "price")
    @Digits(integer = 10 /*precision*/, fraction = 2 /*scale*/)
    private Double price;

    @Positive
    private Integer availability = 0;

    public Item() {

    }

    public Item(String title, String description, Double price, int availability) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.availability = availability;
    }

    public String toJSONString() {
        return "{\"title\" : \"" + title + ", \"description\" : \"" + description + "\", \"price\" : \"" + price + "\", \"availability\" : \"" + availability + "\"}";
      }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setAvailability(Integer availability){
        this.availability = availability;
    }

    public Integer getAvailability(){
        return availability;
    }

    @Override
    public String toString() {
        return "Tutorial [id=" + id + ", title=" + title + ", desc=" + description + ", price=" + price + ", availability=" + availability + "]";
    }

}

